import os
import testinfra.utils.ansible_runner
{%- if cookiecutter.molecule_instance == 'vagrant juniper/vqfx10k-re' %}
import yaml

from os.path import expanduser
from lxml import etree
from jnpr.junos import Device
{%- endif %}

{% if cookiecutter.extra_inventory == 'none' %}
testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')
{%- else %}
testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('molecule_group')
{%- endif %}

{%- if cookiecutter.molecule_instance == 'vagrant juniper/vqfx10k-re' %}
home = expanduser('~')

venv = open(home + '/.cache/molecule/ics-ans-role-{{ cookiecutter.role_name }}/default/inventory/ansible_inventory.yml')
parse_yaml = yaml.load(venv, Loader=yaml.FullLoader)
switch = parse_yaml['all']['hosts']['{{ cookiecutter.role_name }}-default']['ansible_host']
switch_port = parse_yaml['all']['hosts']['{{ cookiecutter.role_name }}-default']['ansible_port']
switch_key = parse_yaml['all']['hosts']['{{ cookiecutter.role_name }}-default']['ansible_private_key_file']
switch_user = parse_yaml['all']['hosts']['{{ cookiecutter.role_name }}-default']['ansible_user']

with Device(host=switch, user=switch_user, ssh_private_key_file=switch_key, port=switch_port) as dev:
    data = dev.rpc.get_config()
    config = etree.tostring(data, encoding='unicode', pretty_print=True)
{%- endif %}


def test_default(host):
    # TODO: implement at least a test
    assert False
