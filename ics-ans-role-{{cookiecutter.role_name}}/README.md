# ics-ans-role-{{ cookiecutter.role_name }}

{{ cookiecutter.description }}.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-{{ cookiecutter.role_name }}
```

## License

BSD 2-clause
