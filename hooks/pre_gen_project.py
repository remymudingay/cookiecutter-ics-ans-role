import re
import sys

ROLE_NAME_REGEX = r'^[a-z][a-z0-9\-]+$'

role_name = '{{ cookiecutter.role_name }}'

if not re.match(ROLE_NAME_REGEX, role_name):
    print('ERROR: "{}" is not a valid role name! It should match "^[a-z][a-z0-9\-]+$"'.format(role_name))
    sys.exit(1)
